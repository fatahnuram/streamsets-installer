# streamsets-installer

Installation wrapper for StreamSets Data Collector

## Notes

This repository is now **working**. However, this repository is still **in development phase**.

## Requirements

Requirements to be fulfilled for this script to be working:

- Linux is running as your OS (can be Ubuntu-based or RHEL-based)
- Systemd is running in your server (instead of SysV)
- StreamSets core tarball is downloaded (download [here](https://streamsets.com/opensource/))
- JDK installed (min. version of 1.8.0)

## How to run

Extract core tarball and rename it into `streamsets-datacollector`

```bash
tar -xzf streamsets-datacollector-core-*.tgz
mv streamsets-datacollector-* streamsets-datacollector
```

Make sure the script is executable

```bash
chmod +x *.sh
```

Run the script with `sudo`

```bash
sudo ./install.sh <install dir>
```

## Limitations

Some limitations for this project until now:

- Only support for Linux (Ubuntu-based or RHEL-based)
- Only support for Systemd until now

## LICENSE

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png "WTFPL")

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

Copyright (C) 2018 Fatah Nur Alam Majid <fatahnuram@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.
