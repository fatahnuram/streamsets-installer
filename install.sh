#!/usr/bin/env bash

# check user priv
if [[ $(whoami) == root ]]; then
    # continue install
    true
else
    # force exit
    echo
    echo "Need root privileges! Aborting.."
    echo
    exit 1
fi

# check args
if [[ $# -gt 1 ]]; then
    # force exit, too much args
    echo
    echo "Too many arguments!"
    echo "Aborting.."
    echo
    exit 1
fi

# check args
if [[ $# -eq 0 ]]; then
    # force exit, args not supplied
    echo
    echo "Could'nt find argument!"
    echo "Aborting.."
    echo
    echo "Usage: sudo ./install.sh <install dir>"
    echo
    exit 1
fi

# verify argument passed
if [[ ! -d ${1} ]]; then
    # force exit, args not supplied
    echo
    echo "Install directory doesn't exist!"
    echo "Aborting.."
    echo
    exit 1
fi

# verify extracted tarball
if [[ ! -d streamsets-datacollector ]]; then
    # force exit, args not supplied
    echo
    echo "StreamSets directory doesn't exist!"
    echo
    echo "You can download them from https://streamsets.com/opensource/"
    echo
    echo "And then extract them into folder named:"
    echo "streamsets-datacollector"
    echo
    echo "Aborting.."
    echo
    exit 1
fi

# check trailing slash in passed argument
if [[ ${1} =~ /$ ]]; then
    # save arg as install dir
    DIR=${1}
else
    # add trailing slash into arg as install dir
    DIR=${1}/
fi

# to-do: verify jdk version

# install details
install() {
    # copy to install dir
    echo "Copying files.."
    cp -r streamsets-datacollector ${1}streamsets-datacollector &>> install.log
    # define new home dir
    HOME_DIR="${1}streamsets-datacollector"
    # copy service files
    cp ${HOME_DIR}/systemd/sdc.* /etc/systemd/system/ &>> install.log
    # add group and user sdc
    echo "Creating user and group.."
    groupadd -r sdc &>> install.log
    useradd -r -d ${HOME_DIR} -g sdc -s /sbin/nologin sdc &>> install.log
    # create directories needed
    echo "Creating directories.."
    mkdir -p /etc/sdc &>> install.log
    mkdir -p /var/log/sdc &>> install.log
    mkdir -p /var/lib/sdc &>> install.log
    # copy contents
    echo "Copying contents.."
    cp -R ${HOME_DIR}/etc/* /etc/sdc &>> install.log
    cp -R ${HOME_DIR}/data/* /var/lib/sdc &>> install.log
    # change ownership
    echo "Changing ownership.."
    chown -R sdc:sdc ${HOME_DIR} &>> install.log
    chown -R sdc:sdc /etc/sdc &>> install.log
    chown -R sdc:sdc /var/log/sdc &>> install.log
    chown -R sdc:sdc /var/lib/sdc &>> install.log
    # running daemon
    echo "Running daemon.."
    systemctl daemon-reload &>> install.log
    systemctl start sdc &>> install.log
    systemctl enable sdc &>> install.log
}

# install process
echo
echo "Installing.."
echo
echo "" > install.log # prepare log
install ${DIR}
echo
echo "Done!"
echo
echo "Now you can login to StreamSets by browsing into http://localhost:18630/"
echo "With user 'admin' and password 'admin'"
echo
echo "You may want to configure your firewall settings to open port 18630 for accessing web UI"
echo